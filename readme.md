# Thrive Hack

## Approach

The approach taken to solving this problem was to build out an architecture that would allow for different "analyzers" to be fed the same data to determine the likelihood of that user being catagorized as a bot.

An analyzer can be anything from simple heuristics checks, machine learning solutions or external API calls.

The architecture chosen allows for many different approaches to be tried and tested, and allows multiple developers to easily work on adding new analysers without interfering with each others work.

## Analyzers

### Validation
Each analyser must determine whether the data passed contains enough information for it to run. This is handled by the "isValid" method. If it is not valid, reasons will be provided.

### Analysis
If the data provided is valid, the analyser must come up with a "bot score" (a mark out of 100 indicating how likely this user is a bot). 

- 100 means that the analyser is 100% certain that this is a bot
- 0 means that the analyser is 100% certain that this is a genuine user
- 50 means that the analyser can provide no useful insight

## API

As a proof of concept, an API has been set up to interface with the bot detection.

### /api/analyse

All parameters are optional:

- UserAgent - The UserAgent string of the user
- IpAddress - The IP address of the user
- FontsList[] - An array of fonts installed on the user's system
- SilverlightVersion - The version of Silverlight installed on the users system
- JavaVersion - The version of Java installed on the users system
- FlashVersion - The version of Flash installed on the users system

Example

GET /api/analyse?UserAgent=Yahoo Ad Monitoring

RESPONSE

{
    "is_bot": true,
    "bot_summary": "This is definitely a bot.",
    "bot_summary_stats": {
        "average_bot_score": 60,
        "highest_bot_score": 100,
        "lowest_bot_score": 50
    },
    "report": [
        {
            "analyzer": "App\\Analyzers\\MacDefaultFonts",
            "is_valid": false,
            "bot_score": 50,
            "reasons": {
                "FontsList": [
                    "The fonts list field is required."
                ]
            }
        },
        {
            "analyzer": "App\\Analyzers\\TimingTooQuickForHumans",
            "is_valid": false,
            "bot_score": 50,
            "reasons": {
                "PageTiming": [
                    "The page timing field is required."
                ],
                "PageTiming.MillisecondsBeforeClickingAd": [
                    "The page timing. milliseconds before clicking ad field is required."
                ]
            }
        },
        {
            "analyzer": "App\\Analyzers\\BlacklistedIpAddressed",
            "is_valid": false,
            "bot_score": 50,
            "reasons": {
                "IpAddress": [
                    "The ip address field is required."
                ]
            }
        },
        {
            "analyzer": "App\\Analyzers\\IdentifyCrawlers",
            "is_valid": true,
            "bot_score": 100,
            "reasons": [
                "The UserAgent 'Yahoo Ad monitoring' is a known bot."
            ]
        },
        {
            "analyzer": "App\\Analyzers\\IdentifyCommonWindowsPlugins",
            "is_valid": true,
            "bot_score": 50,
            "reasons": [
                "The OperatingSystem '' wasn't identified as a Windows."
            ]
        }
    ]
}

GET /api/analyse?UserAgent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36&FontsList[]=Zapfino

{
    "is_bot": true,
    "bot_summary": "We can say with 58 percent accuracy that this IS a bot",
    "bot_summary_stats": {
        "average_bot_score": 58.333333333333336,
        "highest_bot_score": 75,
        "lowest_bot_score": 50
    },
    "report": [
        {
            "analyzer": "App\\Analyzers\\TimingTooQuickForHumans",
            "is_valid": false,
            "bot_score": 50,
            "reasons": {
                "PageTiming": [
                    "The page timing field is required."
                ],
                "PageTiming.MillisecondsBeforeClickingAd": [
                    "The page timing. milliseconds before clicking ad field is required."
                ]
            }
        },
        {
            "analyzer": "App\\Analyzers\\BlacklistedIpAddressed",
            "is_valid": false,
            "bot_score": 50,
            "reasons": {
                "IpAddress": [
                    "The ip address field is required."
                ]
            }
        },
        {
            "analyzer": "App\\Analyzers\\IdentifyCrawlers",
            "is_valid": true,
            "bot_score": 50,
            "reasons": [
                "The UserAgent 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36' isn't a known bot."
            ]
        },
        {
            "analyzer": "App\\Analyzers\\MacDefaultFonts",
            "is_valid": true,
            "bot_score": 75,
            "reasons": [
                "The agent has 117 default OS fonts missing."
            ]
        },
        {
            "analyzer": "App\\Analyzers\\IdentifyCommonWindowsPlugins",
            "is_valid": true,
            "bot_score": 50,
            "reasons": [
                "The OperatingSystem 'Mac OS X' wasn't identified as a Windows."
            ]
        }
    ]
}
