<?php

namespace App\Http\Controllers;

use App\AnalyzerFactory;
use Illuminate\Http\Request;

class AnalysisController extends Controller
{
    public function index(Request $request)
    {
        $analyzer = app(AnalyzerFactory::class)->analyze($request->all());

        $report = $analyzer->getReport();

        return [
            'is_bot' => $analyzer->isBot(),
            'bot_summary' => $analyzer->getBotSummaryInText(),
            'bot_summary_stats' => $analyzer->getBotSummary(),
            'report' => $report,
        ];
    }
}
