<?php

namespace App;

class AnalyzerFactory
{
    protected $analyzers;

    public function __construct($analyzers)
    {
        $this->analyzers = $analyzers;
    }

    public function analyze(array $data)
    {
        return (new Analyzer($this->analyzers, $data))->run();
    }
}
