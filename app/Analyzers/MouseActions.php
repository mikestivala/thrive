<?php

namespace App\Analyzers;

use App\Analyzers\Contracts\Analyzer;

class MouseActions extends BaseAnalyzer implements Analyzer
{
    public function getRules()
    {
        return [
            'MouseActions' => 'required|array',
            'MouseActions.*.Type' => 'required|in:click,scroll',
            'MouseActions.*.MillisecondsAfterPageLoad' => 'required|numeric',
            'MouseActions.*.ClickX' => 'required_if:MouseActions.*.Type,click',
            'MouseActions.*.ClickY' => 'required_if:MouseActions.*.Type,click',
            'MouseActions.*.ScrollY' => 'required_if:MouseActions.*.Type,scroll',
        ];
    }

    public function runAnalysis()
    {
        // POC... :)

        return $this->markAsUnknown();
    }
}
