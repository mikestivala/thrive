<?php

namespace App\Analyzers;

use Validator;
use App\Analyzers\Contracts\Analyzer;

abstract class BaseAnalyzer implements Analyzer
{
    protected $data;
    protected $botScore;
    protected $reasons;

    abstract protected function runAnalysis();

    /**
     * Create an instance of the analyzer.
     * This can be overridden by analyzers to inject
     * any other dependencies into the constructor.
     *
     * @param  array  $data
     * @return self
     */
    public static function create(array $data) : Analyzer
    {
        $class = new static();
        $class->data = $data;

        return $class;
    }

    public function getRules()
    {
        return [];
    }

    /**
     * Validates the data input for the analyzer
     *
     * @return boolean
     */
    public function isValid() : bool
    {
        $validator = Validator::make($this->data, $this->getRules());

        if ($validator->passes()) {
            return true;
        }

        $this->reasons = $validator->messages()->toArray();

        return false;
    }

    public function botScore() : int
    {
        if (! $this->botScore) {
            $this->runAnalysis();
        }

        return $this->botScore;
    }

    public function reasons() : array
    {
        if (! $this->reasons) {
            $this->runAnalysis();
        }

        return $this->reasons;
    }

    protected function addReason(string $reason)
    {
        $this->reasons[] = $reason;

        return $this;
    }

    protected function markBotScore(int $botScore, string $reason)
    {
        $this->botScore = $botScore;
        $this->addReason($reason);

        return $this;
    }

    protected function markAsUnknown(string $reason = "The result of this test is inconclusive")
    {
        return $this->markBotScore(50, $reason);
    }

    protected function markAsLikelyBot(string $reason)
    {
        return $this->markBotScore(75, $reason);
    }

    protected function markAsLikelyUser(string $reason)
    {
        return $this->markBotScore(25, $reason);
    }
}
