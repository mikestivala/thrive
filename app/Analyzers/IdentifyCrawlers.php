<?php

namespace App\Analyzers;

use App\Analyzers\Contracts\Analyzer;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class IdentifyCrawlers extends BaseAnalyzer implements Analyzer
{
    public function getRules()
    {
        return [
            'UserAgent' => 'required|string',
        ];
    }

    public function runAnalysis()
    {
        $crawlerDetect = new CrawlerDetect;

        // If the UserAgent is a known bot, we'll
        // instantly classify it as such.
        if ($crawlerDetect->isCrawler($this->data['UserAgent'])) {
            return $this->markBotScore(
                100,
                sprintf("The UserAgent '%s' is a known crawler.", $this->data['UserAgent'])
            );
        }

        // We know this isn't a known bot, but can't
        // conclusively confirm that it is a real user
        $this->markAsUnknown(sprintf("The UserAgent '%s' isn't a known crawler.", $this->data['UserAgent']));
    }
}
