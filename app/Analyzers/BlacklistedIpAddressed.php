<?php

namespace App\Analyzers;

use GuzzleHttp\Client;
use App\Analyzers\Contracts\Analyzer;

class BlacklistedIpAddressed extends BaseAnalyzer implements Analyzer
{
    public function getRules()
    {
        return [
            'IpAddress' => 'required|ip',
        ];
    }

    public function runAnalysis()
    {
        $client = new Client();

        try {
            $result = $client->request('POST', 'https://neutrinoapi.com/ip-blocklist', array_merge([
                'ip' => $this->data['IpAddress']
            ], config('services.neutrino')));
        } catch (\Exception $e) {
            return $this->markAsUnknown("The neutrino API couldn't be reached.");
        }

        $result = json_decode($result->getBody());

        if ($result->{'is-listed'}) {
            return $this->markBotScore(100, "This IP was found on the neutrino blocklist.");
        }

        return $this->markAsUnknown();
    }
}
