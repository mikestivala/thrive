<?php

namespace App\Analyzers;

use App\Analyzers\Contracts\Analyzer;

class TimingTooQuickForHumans extends BaseAnalyzer implements Analyzer
{
    public function getRules()
    {
        return [
            'PageTiming' => 'required|array',
            'PageTiming.MillisecondsBeforeClickingAd' => 'required|numeric',
        ];
    }

    public function runAnalysis()
    {
        // Ads clicked unrealistically quickly could indicate a bot...
        if ($this->data['PageTiming.MillisecondsBeforeClickingAd'] < 250) {
            return $this->markAsLikelyBot(sprintf("An ad on this page was clicked within %d milliseconds", $this->data['PageTiming.MillisecondsBeforeClickingAd']));
        }

        // Ads clicked after a long time (5minutes+) could indicate a real user...
        if ($this->data['PageTiming.MillisecondsBeforeClickingAd'] > 5 * 60 * 1000) {
            return $this->markAsLikelyUser(sprintf("An ad on this page was clicked after %d milliseconds", $this->data['PageTiming.MillisecondsBeforeClickingAd']));
        }

        return $this->markAsUnknown();
    }
}
