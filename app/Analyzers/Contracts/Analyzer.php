<?php

namespace App\Analyzers\Contracts;

interface Analyzer
{
    /**
     * Create an instance of the analyzer given
     * the input data.
     *
     * @param  array  $data
     * @return self
     */
    public static function create(array $data) : self;

    /**
     * Determines whether the data given
     * to the analyzer can be used for
     * analysis.
     *
     * @return boolean
     */
    public function isValid() : bool;

    /**
     * Scores the given data out of 100
     * 100 = definitely a bot
     * 0 = definitely a user
     *
     * @return int
     */
    public function botScore() : int;

    /**
     * Return a list of reasons for coming up
     * with the botScore. This text is just
     * used as a summary of the analyzer.
     *
     * @return array
     */
    public function reasons() : array;
}
