<?php

namespace App\Analyzers;

use App\OSDetector;
use App\Analyzers\Contracts\Analyzer;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class MacDefaultFonts extends BaseAnalyzer implements Analyzer
{
    public static $macFonts = [
        'Al Bayan',
        'American Typewrite',
        'Andalé Mon',
        'Apple Casual',
        'Apple Chancer',
        'Apple Garamond',
        'Apple Gothic',
        'Apple LiGothic',
        'Apple LiSung',
        'Apple Myungjo',
        'Apple Symbols',
        '.AquaKana',
        'Arial',
        'Arial Hebrew',
        'Ayuthaya',
        'Baghdad',
        'Baskervill',
        'Beijing',
        'BiauKai',
        'Big Caslon',
        'Brush',
        'Chalkboard',
        'Chalkduste',
        'Charcoal',
        'Charcoal CY',
        'Chicag',
        'Cochin',
        'Comic Sans',
        'Cooper',
        'Copperplat',
        'Corsiva Hebrew',
        'Courier',
        'Courier New',
        'DecoType Naskh',
        'Devanagari',
        'Didot',
        'Euphemia UCAS',
        'Futura',
        'Gadget',
        'Geeza Pro',
        'Geezah',
        'Geneva',
        'Geneva CY',
        'Georgi',
        'Gill Sans',
        'Gujarati',
        'Gung Seoche',
        'Gurmukhi',
        'Hangangche',
        'HeadlineA',
        'Hei',
        'Helvetica',
        'Helvetica CY',
        'Helvetica Neue',
        'Herculanum',
        'Hiragino Kaku Gothic Pro',
        'Hiragino Kaku Gothic ProN',
        'Hiragino Kaku Gothic Std',
        'Hiragino Kaku Gothic StdN',
        'Hiragino Maru Gothic Pro',
        'Hiragino Maru Gothic ProN',
        'Hiragino Mincho Pro',
        'Hiragino Mincho ProN',
        'Hoefler Text',
        'Inai Mathi',
        'Impact',
        'Jung Gothic',
        'Kai',
        'Keyboard',
        'Krungthep',
        'KufiStandard GK',
        'LastResort',
        'LiHei Pro',
        'LiSong Pro',
        'Lucida Grande',
        'Marker Fel',
        'Menlo',
        'Monaco',
        'Monaco CY',
        'Mshtakan',
        'Nadeem',
        'New Penini',
        'New York',
        'NISC GB18030',
        'Optima',
        'Osaka',
        'Palatino',
        'Papyru',
        'PC Myungjo',
        'Pilgiche',
        'Plantagenet Cherokee',
        'Raanana',
        'Sand',
        'Sathu',
        'Seoul',
        'Shin Myungjo Neue',
        'Silom',
        'Skia',
        'Song',
        'ST FangSong',
        'ST Heiti',
        'ST Kaiti',
        'ST Song',
        'Symbol',
        'Tae Graphic',
        'Tahoma',
        'Taipei',
        'Techno',
        'Textil',
        'Thonburi',
        'Times',
        'Times CY',
        'Times New Roma',
        'Trebuchet MS',
        'Verdan',
        'Zapf Chancery',
        'Zapf Dingbats',
        'Zapfino',
    ];

    public function getRules()
    {
        return [
            'UserAgent' => 'required|string',
            'FontsList' => 'required|array',
        ];
    }

    public function runAnalysis()
    {
        $operatingSystem = new OSDetector($this->data['UserAgent']);

        if (! $operatingSystem->isMac()) {
            return $this->markAsUnknown(sprintf("The OperatingSystem '%s' wasn't identified as a mac.", $operatingSystem->getOS()));
        }

        $defaultFontsMissing = collect(self::$macFonts)->diff($this->data['FontsList']);

        // Our heuristic is that if more than 75% of the default system fonts
        // are present, then the agent is likely a real user. If less than 25%
        // of the default fonts are present, then the agent is likely a bot.
        if ($defaultFontsMissing->count() < count(self::$macFonts) * .25) {
            return $this->markAsLikelyUser(sprintf("The agent has %d default OS fonts missing.", $defaultFontsMissing->count()));
        }

        if ($defaultFontsMissing->count() > count(self::$macFonts) * .75) {
            return $this->markAsLikelyBot(sprintf("The agent has %d default OS fonts missing.", $defaultFontsMissing->count()));
        }

        $this->markAsUnknown();
    }
}
