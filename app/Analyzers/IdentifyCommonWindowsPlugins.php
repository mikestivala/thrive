<?php

namespace App\Analyzers;

use App\OSDetector;
use App\Analyzers\Contracts\Analyzer;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class IdentifyCommonWindowsPlugins extends BaseAnalyzer implements Analyzer
{
    public function getRules()
    {
        return [
            'UserAgent' => 'required|string',
            'SilverlightVersion' => 'nullable|string',
            'JavaVersion' => 'nullable|string',
            'FlashVersion' => 'nullable|string',
        ];
    }

    public function runAnalysis()
    {
        $operatingSystem = new OSDetector($this->data['UserAgent']);

        if (! $operatingSystem->isWindows()) {
            return $this->markAsUnknown(sprintf("The OperatingSystem '%s' wasn't identified as a Windows.", $operatingSystem->getOS()));
        }

        // It's less likely that a bot will execute silverlight, java and flash
        // since it will slow down crawling considerably. We'll use this
        // heuristic here to mark agents as likely users.
        if ($this->data['SilverlightVersion'] && $this->data['JavaVersion'] && $this->data['FlashVersion']) {
            return $this->markAsLikelyUser("Silverlight, Java and Flash were all detected.");
        }

        // Not having the mentioned software installed doesn't necessarily
        // mean that this is a bot. Research can be done to determine
        // if any meaning can be associated to the lack of this
        // software being present on Windows machines.
        return $this->markAsUnknown();
    }
}
