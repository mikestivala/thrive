<?php

namespace App\Console\Commands;

use App\AnalyzerFactory;
use Illuminate\Support\Arr;
use Illuminate\Console\Command;

class Analyze extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thrive:analyze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analyze some sample input data and create a report on it.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'UserAgent' => 'Yahoo Ad monitoring',
            // 'IpAddress' => '83.142.248.5',
        ];

        $this->comment('Running analysis: ');
        $this->info(json_encode($data));
        $this->comment('---');
        $analyzer = app(AnalyzerFactory::class)->analyze($data);

        $report = $analyzer->getReport();

        $this->comment("Bot Score (100 = bot, 0 = user, 50 = inconclusive)");

        $headers = [
            'Analyzer',
            'Valid Input',
            'Bot Score',
            'Reasons',
        ];

        $report = array_map(function ($reportItem) {
            $reportItem['is_valid'] = $reportItem['is_valid'] ? '✅' : '❌';
            $reportItem['reasons'] = Arr::flatten($reportItem['reasons'], INF);
            $reportItem['reasons'] = implode(", ", $reportItem['reasons']);
            return $reportItem;
        }, $report);


        $this->table($headers, $report);

        if ($analyzer->getBotSummary()['highest_bot_score'] == 100) {
            return $this->comment("Conclusion: this is definitely a bot.");
        }

        if ($analyzer->getBotSummary()['lowest_bot_score'] == 0) {
            return $this->comment("Conclusion: this is definitely a user.");
        }

        return $this->comment(sprintf("Conclusion: We can say with %d% accuracy that this %s a bot", $analyzer->getBotSummary()['average_bot_score'], $analyzer->getBotSummary()['average_bot_score'] > 50));
    }
}
