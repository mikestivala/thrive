<?php

namespace App;

class Analyzer
{
    /**
     * The collection of analyzer classes
     *
     * @var array
     */
    protected $analyzers;

    /**
     * The data of inputs collected
     * from the user that we are analyzing
     *
     * @var array
     */
    protected $data;

    /**
     * The report generated about this data
     *
     * @var array
     */
    protected $report;

    public function __construct(array $analyzers, array $data)
    {
        $this->analyzers = collect($analyzers);
        $this->data = $data;
    }

    /**
     * Here we're going to run through all the analyzers and get their individual scores.
     *
     * Eventually this step can be optimized by introducing a queueing system
     * to allow the different analyzers to be run asynchronously.
     * @return self
     */
    public function run()
    {
        // Instantiate each analyzer...
        $instances = $this->analyzers->map(function ($analyzer) {
            return $analyzer::create($this->data);
        });

        // Add report data as to why certain analyzers couldn't be run...
        $instances->reject->isValid()->each(function ($instance) {
            $this->report[] = [
                'analyzer' => get_class($instance),
                'is_valid' => false,
                'bot_score' => 50,
                'reasons' => $instance->reasons(),
            ];
        });

        // Add report data returned by the analyzers that could run...
        $instances->filter->isValid()->each(function ($instance) {
            $this->report[] = [
                'analyzer' => get_class($instance),
                'is_valid' => true,
                'bot_score' => $instance->botScore(),
                'reasons' => $instance->reasons(),
            ];
        });

        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getBotSummary()
    {
        $report = collect($this->report)->filter->is_valid;

        return [
            'average_bot_score' => $report->average('bot_score'),
            'highest_bot_score' => $report->max('bot_score'),
            'lowest_bot_score' => $report->min('bot_score'),
        ];
    }

    public function getBotSummaryInText()
    {
        if ($this->getBotSummary()['highest_bot_score'] == 100) {
            return "This is definitely a bot.";
        }

        if ($this->getBotSummary()['lowest_bot_score'] == 0) {
            return "This is definitely a user.";
        }

        if ($this->getBotSummary()['average_bot_score'] > 50) {
            return sprintf("We can say with %d percent accuracy that this IS a bot", $this->getBotSummary()['average_bot_score']);
        }

        return sprintf("We can say with %d percent accuracy that this IS NOT a bot", 100 - $this->getBotSummary()['average_bot_score']);
    }

    public function isBot()
    {
        // One of the analyzers has identified that this is definitely a bot...
        if ($this->getBotSummary()['highest_bot_score'] === 100) {
            return true;
        }

        // One of the analyzers has identified that this is definitely a user...
        if ($this->getBotSummary()['lowest_bot_score'] === 0) {
            return false;
        }

        // Otherewise, use the average result from the
        // analyzers to determine whether this is a bot
        return $this->getBotSummary()['average_bot_score'] > 50;
    }
}
