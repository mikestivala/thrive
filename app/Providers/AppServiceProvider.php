<?php

namespace App\Providers;

use App\AnalyzerFactory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AnalyzerFactory::class, function () {
            // Here we're adding the list of all the analyzers
            // that we are plugging into our analysis system.
            return new AnalyzerFactory([
                \App\Analyzers\IdentifyCrawlers::class,
                \App\Analyzers\MacDefaultFonts::class,
                \App\Analyzers\IdentifyCommonWindowsPlugins::class,
                \App\Analyzers\TimingTooQuickForHumans::class,
                \App\Analyzers\BlacklistedIpAddressed::class,
            ]);
        });
    }
}
