<?php

namespace Tests\Feature\Analyzers;

use Tests\TestCase;
use App\Analyzers\MacDefaultFonts;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MacDefaultFontsTest extends TestCase
{
    /** @test */
    function macs_with_default_fonts_installed_look_like_users()
    {
        $analyzer = MacDefaultFonts::create([
            'UserAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'FontsList' => MacDefaultFonts::$macFonts,
        ]);

        $this->assertEquals(25, $analyzer->botScore());
    }

    /** @test */
    function macs_with_no_fonts_installed_look_like_bots()
    {
        $analyzer = MacDefaultFonts::create([
            'UserAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'FontsList' => [],
        ]);

        $this->assertEquals(75, $analyzer->botScore());
    }
}
