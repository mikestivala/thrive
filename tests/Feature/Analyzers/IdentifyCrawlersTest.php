<?php

namespace Tests\Feature\Analyzers;

use Tests\TestCase;
use App\Analyzers\IdentifyCrawlers;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IdentifyCrawlersTest extends TestCase
{
    /** @test */
    function it_identifies_yahoo_bot()
    {
        $analyzer = IdentifyCrawlers::create([
            'UserAgent' => 'Yahoo Ad monitoring'
        ]);

        $this->assertEquals(100, $analyzer->botScore());
        $this->assertNotEmpty($analyzer->reasons());
    }

    /** @test */
    function it_marks_as_invalid()
    {
        $analyzer = IdentifyCrawlers::create([
            'Invalid' => 'Yahoo Ad monitoring'
        ]);

        $this->assertFalse($analyzer->isValid());
    }
}
