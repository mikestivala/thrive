<?php

namespace Tests\Feature\Analyzers;

use Tests\TestCase;
use App\Analyzers\MouseActions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MouseActionsTest extends TestCase
{
    /** @test */
    function it_accepts_valid_data()
    {
        $analyzer = MouseActions::create([
            'MouseActions' => [
                [
                    'Type' => 'scroll',
                    'MillisecondsAfterPageLoad' => 300,
                    'ScrollY' => 800,
                ],
                [
                    'Type' => 'click',
                    'MillisecondsAfterPageLoad' => 382,
                    'ClickX' => 800,
                    'ClickY' => 400,
                ],
            ],
        ]);

        $this->assertTrue($analyzer->isValid());
        $this->assertNotEmpty($analyzer->reasons());
    }
}
